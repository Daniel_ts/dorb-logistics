@extends('layouts.app')

@section('content')
@if(isset($trips))
    <h1>{{$trips->Date}}</h1>
    <p>Vrachtwagen kenteken: {{$trucks->License_Plate}}</p>
    @foreach($orders as $order)
        @php($x=0)
        <div class="card">
            <p>Stad: {{$order->City}}</p>
            <p>Straat: {{$order->Street}}</p>
            <p>Huisnummer: {{$order->Housenumber}}</p>
            <p>ZIP: {{$order->ZIPCode}}</p>
            @foreach($pallets as $pallet)
                @if($pallet->Order_Id == $order->Order_Id)
                    @php($x++)              
                @endif
            @endforeach
            <p>Pallets: {{$x}}</p>
        </div>
        <br>
    @endforeach
@else 
    <p>Geen trip</p>
@endif
@endsection
@extends('layouts.app')

@section('content')
    <h1>{{$date}}</h1>
    <div class="row">
        <div class="col-6">
            @foreach($orders as $order)
                @if(!isset($order->Trip_Id))
                    <div class="card row" style="width: 25rem;">
                        <div class="card-body">
                            <p>Stad: {{$order->City}}</p>
                            @php 
                                $x=1
                            @endphp
                            @foreach($pallets as $pallet)
                                @if($order->Order_Id == $pallet->Order_Id)
                                    <div class="card" style="width: 10rem; float: left;">
                                        <p>Pallet {{$x}}</p>
                                        <p>Contents {{$pallet->Contents}}</p>
                                        <p>Weight {{$pallet->Weight}}</p>
                                        @php 
                                            $x++
                                        @endphp
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="col-6">
            @php
                $x = 1;
            @endphp
            @foreach($trips as $trip)
                <div class="card row" style="width: 25rem; float: right">
                    <div class="card-body">
                        <p>Trip: {{$x}}</p>
                        <p>Chauffeur: {{$trip->Driver_Id}}</p>
                        <p>Vrachtwagen: {{$trip->Truck_Id}}</p>
                        @foreach($orders as $order)
                            @if($order->Trip_Id == $trip->Trip_Id)
                                <div class="card" style="width: 10rem; float: left;">
                                    <p>Order: {{$order->Order_Id}}</p>
                                    <p>Stad: {{$order->City}}</p>
                                    @php 
                                        $count = 0;
                                        foreach($pallets as $pallet){
                                            if($pallet->Order_Id == $order->Order_Id){
                                                $count++;
                                            }
                                        }
                                    @endphp
                                    <p>Pallets: {{$count}}</p>
                                    @php
                                        $x++
                                    @endphp
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a href="/trips/{{$trip->Trip_Id}}/edit?date={{$date}}" class="btn btn-primary">Edit</a>
                </div>
            @endforeach
        </div>
    <a href="/trips/create?date={{$date}}" class="btn btn-primary">Add Trip</a>
    </div>
@endsection
@extends('layouts.app')

@section('content')
{!! Form::open(['action'=>['TripController@update', $trip->Trip_Id], 'method' => 'PUT']) !!}

    <h1>{{$date}}</h1>
    <div class="row">
        <div class="col-6">
            @foreach($orders as $order)
                @if(!isset($order->Trip_Id) || $order->Trip_Id == $trip->Trip_Id)
                    <div class="card row" style="width: 25rem;">
                        <div class="card-body">
                            <p>Stad: {{$order->City}}</p>
                            @php 
                                $x=1
                            @endphp
                            @foreach($pallets as $pallet)
                                @if($order->Order_Id == $pallet->Order_Id)
                                    <div class="card" style="width: 11rem; float: left;">
                                        <p>Pallet {{$x}}</p>
                                        <p>Contents {{$pallet->Contents}}</p>
                                        <p>Weight {{$pallet->Weight}}</p>
                                        @php 
                                            $x++
                                        @endphp
                                    </div>
                                @endif
                            @endforeach
                            <input type="checkbox" name="orders[]" value="{{$order->Order_Id}}" @if($order->Trip_Id == $trip->Trip_Id) checked @endif>  Voeg toe
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="col-6">
            
            <select class="form-control" name="driver">
                @foreach($drivers as $driver)
                    <option value="{{$driver->User_Id}},{{$driver->License_Type}},{{$driver->Workdays}}" @if($driver->User_Id == $trip->Driver_Id) selected @endif>{{$driver->Username}}  ({{$driver->License_Type}})</option>
                @endforeach
            </select>
            <select class="form-control" name="truck">
                @foreach($trucks as $truck)
                     <option value="{{$truck->Truck_Id}},{{$truck->Type}}"@if($truck->Truck_Id == $trip->Truck_Id) selected @endif>{{$truck->License_Plate}}  (@if($truck->Type == 'XF 430 FTM')<p>D</p>@else<p>C</p>@endif)</option>
                @endforeach
            </select>
            {{Form::hidden('predriver', $trip->Driver_Id)}}
            {{Form::hidden('pretruck', $trip->Truck_Id)}}
            {{Form::hidden('id', $trip->Trip_Id)}}
            {{Form::hidden('date', $date)}}
            {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
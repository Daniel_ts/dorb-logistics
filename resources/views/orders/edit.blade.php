@extends('layouts.app')

@section('content')
    <h1>Edit Order</h1>
    {!! Form::open(['action'=>['OrderController@update', $order->Order_Id], 'method' => 'PUT']) !!}
    <div class="form-group">
            {{Form::label('city', 'Stad')}}
            {{Form::text('city', $order->City, ['class' => 'form-control', 'placeholder' => 'Stad'])}}
        </div>
        <div class="form-group">
            {{Form::label('street', 'Straat')}}
            {{Form::text('street', $order->Street, ['class' => 'form-control', 'placeholder' => 'Straat'])}}
        </div>
        <div class="form-group">
            {{Form::label('housenumber', 'Huisnummer')}}
            {{Form::text('housenumber', $order->Housenumber, ['class' => 'form-control', 'placeholder' => 'Huisnummer'])}}
        </div>
        <div class="form-group">
                {{Form::label('ZIPCode', 'ZIPCode')}}
                {{Form::text('ZIPCode', $order->ZIPCode, ['class' => 'form-control', 'placeholder' => 'ZIPCode'])}}
            </div>
        <div class="form-group">
            {{Form::label('date', 'Datum')}}
            {{Form::text('date', $order->Date, ['class' => 'form-control', 'placeholder' => 'Datum'])}}
        </div>

        <div class="wrapper">
            <button class="add_field_button btn btn-dark">add</button>
            <br>
            <br>
            @php ($i = 1)
            @foreach ($pallets as $pallet)
            <div class='form-group border border-dark'>
                Pallet {{$i}}
                @php ($i++)
                <br>
                {{Form::label('contents', 'Contents')}}
                {{Form::text('PalletContents[]', $pallet->Contents, ['class' => 'form-control', 'placeholder' => 'Contents'])}}
                {{Form::label('weight', 'Weight')}}
                {{Form::text('PalletWeight[]', $pallet->Weight, ['class' => 'form-control', 'placeholder' => 'Weight'])}}
                {{Form::hidden('Id[]', $pallet->Pallet_Id)}}
                {{Form::hidden('new_old[]', 'old')}}
            </div>
            @endforeach
            <input type='hidden' value='{{$i}}' class='i'>
        </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
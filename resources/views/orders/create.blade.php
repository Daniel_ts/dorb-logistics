@extends('layouts.app')

@section('content')
    <h1>Create Order</h1>
    {!! Form::open(['action'=>'OrderController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('city', 'Stad')}}
        {{Form::text('city', '', ['class' => 'form-control', 'placeholder' => 'Stad'])}}
    </div>
    <div class="form-group">
        {{Form::label('street', 'Straat')}}
        {{Form::text('street', '', ['class' => 'form-control', 'placeholder' => 'Straat'])}}
    </div>
    <div class="form-group">
        {{Form::label('housenumber', 'Huisnummer')}}
        {{Form::text('housenumber', '', ['class' => 'form-control', 'placeholder' => 'Huisnummer'])}}
    </div>
    <div class="form-group">
        {{Form::label('ZIPCode', 'ZIPCode')}}
        {{Form::text('ZIPCode', '', ['class' => 'form-control', 'placeholder' => 'ZIPCode'])}}
    </div>
    <div class="form-group">
        {{Form::label('date', 'Datum')}}
        {{Form::text('date', '', ['class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'])}}
    </div>

    <div class="wrapper">
        <button class="add_field_button btn btn-dark">add</button>
        <br>
        <br>
    </div>
    <input type='hidden' value='0' class='i'>
    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    
@endsection
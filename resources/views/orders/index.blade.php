@extends('layouts.app')

@section('content')
    <h1>Your Orders</h1>
        @foreach($orders as $order)
            <div class="card">
                <h3><a href="/orders/{{$order->Order_Id}}">Order: {{$order->Order_Id}}</a></h3>
                <p>{{$order->City}}</p>
                <p>{{$order->Date}}</p>
            </div>
        @endforeach
@endsection
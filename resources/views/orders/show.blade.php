@extends('layouts.app')

@section('content')
    @if(Auth::id() == $order->User_Id)
    
        <h3>Order: {{$order->Order_Id}}</h3>
        <p>Stad: {{$order->City}}</p>
        <p>Straat: {{$order->Street}}</p>
        <p>Huisnummer: {{$order->Housenumber}}</p>
        <p>ZIPCode: {{$order->ZIPCode}}</p>
        <p>Datum: {{$order->Date}}</p>
        @php ($i=1)
        @foreach ($pallets as $pallet)
        <div class='form-group border border-dark'>
            Pallet {{$i}}
            @php ($i++)
            <br>
            <p>Content: {{$pallet->Contents}}</p>
            <p>Weight: {{$pallet->Weight}}</p>
            {!!Form::open(['action' => ['PalletController@destroy', $pallet->Pallet_Id], 'method' => 'POST'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::hidden('OrderId', $order->Order_Id)}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        </div>
        <input type='hidden' value='{{$i}}' class='i'>
        @endforeach

        <hr>
        <a href="/orders/{{$order->Order_Id}}/edit" class="btn btn-primary">Edit</a>

        {!!Form::open(['action' => ['OrderController@destroy', $order->Order_Id], 'method' => 'POST', 'class' => 'float-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!!Form::close()!!}
    @endif
@endsection
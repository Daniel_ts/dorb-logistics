@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="Username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="Username" type="text" class="form-control{{ $errors->has('Username') ? ' is-invalid' : '' }}" name="Username" value="{{ old('Username') }}" required autofocus>

                                @if ($errors->has('Username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="First-Name" class="col-md-4 col-form-label text-md-right">{{ __('Voornaam') }}</label>
    
                            <div class="col-md-6">
                                <input id="First-Name" type="text" class="form-control{{ $errors->has('First-Name') ? ' is-invalid' : '' }}" name="First-Name" value="{{ old('First-Name') }}"  autofocus>
    
                                @if ($errors->has('First-Name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('First-Name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Surname-Prefix" class="col-md-4 col-form-label text-md-right">{{ __('Tussenvoegsel') }}</label>

                            <div class="col-md-6">
                                <input id="Surname-Prefix" type="text" class="form-control{{ $errors->has('Surname-Prefix') ? ' is-invalid' : '' }}" name="Surname-Prefix" value="{{ old('Surname-Prefix') }}"  autofocus>

                                @if ($errors->has('Surname-Prefix'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Surname-Prefix') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Surname" class="col-md-4 col-form-label text-md-right">{{ __('Achternaam') }}</label>

                            <div class="col-md-6">
                                <input id="Surname" type="text" class="form-control{{ $errors->has('Surname') ? ' is-invalid' : '' }}" name="Surname" value="{{ old('Surname') }}"  autofocus>

                                @if ($errors->has('Surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Tell" class="col-md-4 col-form-label text-md-right">{{ __('Telefoon Nummer') }}</label>

                            <div class="col-md-6">
                                <input id="Tell" type="text" class="form-control{{ $errors->has('Tell') ? ' is-invalid' : '' }}" name="Tell" value="{{ old('Tell') }}"  autofocus>

                                @if ($errors->has('Tell'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Tell') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Credit_Number" class="col-md-4 col-form-label text-md-right">{{ __('IBan') }}</label>

                            <div class="col-md-6">
                                <input id="Credit_Number" type="text" class="form-control{{ $errors->has('Credit_Number') ? ' is-invalid' : '' }}" name="Credit_Number" value="{{ old('Credit_Number') }}"  autofocus>

                                @if ($errors->has('Credit_Number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Credit_Number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Street" class="col-md-4 col-form-label text-md-right">{{ __('Straat') }}</label>

                            <div class="col-md-6">
                                <input id="Street" type="text" class="form-control{{ $errors->has('Street') ? ' is-invalid' : '' }}" name="Street" value="{{ old('Street') }}"  autofocus>

                                @if ($errors->has('Street'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="HouseNumber" class="col-md-4 col-form-label text-md-right">{{ __('Huisnummer') }}</label>

                            <div class="col-md-6">
                                <input id="HouseNumber" type="text" class="form-control{{ $errors->has('HouseNumber') ? ' is-invalid' : '' }}" name="HouseNumber" value="{{ old('HouseNumber') }}"  autofocus>

                                @if ($errors->has('HouseNumber'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('HouseNumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="City" class="col-md-4 col-form-label text-md-right">{{ __('Stad') }}</label>

                            <div class="col-md-6">
                                <input id="City" type="text" class="form-control{{ $errors->has('City') ? ' is-invalid' : '' }}" name="City" value="{{ old('City') }}"  autofocus>

                                @if ($errors->has('City'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('City') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ZIPCode" class="col-md-4 col-form-label text-md-right">{{ __('ZIPCode') }}</label>

                            <div class="col-md-6">
                                <input id="ZIPCode" type="text" class="form-control{{ $errors->has('ZIPCode') ? ' is-invalid' : '' }}" name="ZIPCode" value="{{ old('ZIPCode') }}"  autofocus>

                                @if ($errors->has('ZIPCode'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ZIPCode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

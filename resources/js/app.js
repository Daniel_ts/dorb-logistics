
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./fullcalendar');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});

$(document).ready(function(){
    var x = $('.i').val();
    $('.add_field_button').click(function(e){
        e.preventDefault();
        if(x<30){
          $('.wrapper').append("<div class='form-group border border-dark'>Pallet "+x+"<br>Contents <input type='text' class='form-control' placeholder='Contents' name='PalletContents[]'>Weight <input type='text' class='form-control' placeholder='Weight' name='PalletWeight[]'><input type='hidden' value='new' class='new_old[]'><input type='button' value='Verwijder' class='RemoveButton btn btn-danger'</div>");
        x++;
        }
    });
    $('.wrapper').on("click",".RemoveButton", function(e){
        e.preventDefault();
        $(this).parent().remove();
        x--;
    });
});


$(function() {
    $('#calendar').fullCalendar({
      themeSystem: 'bootstrap4',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
      weekNumbers: true,
      eventLimit: true, // allow "more" link when too many events
      events: 'http://dorbs.test/calendar',
      
      
      
      dayClick: function(date, jsEvent, view) {
        
        window.location.href = 'calendar/'+date.format();
      }
    });
  
  });
  
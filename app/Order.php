<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Table Name
    protected $table = 'orders';
    public $primaryKey = 'Order_Id';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

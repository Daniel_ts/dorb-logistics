<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trucks extends Model
{
    // Table Name
    protected $table = 'trucks';
    public $primaryKey = 'Truck_Id';
    public $timestamps = false;
}
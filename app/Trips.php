<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trips extends Model
{
    // Table Name
    protected $table = 'trips';
    public $primaryKey = 'Trip_Id';
    public $timestamps = false;

    
}
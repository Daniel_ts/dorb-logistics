<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pallet extends Model
{
    // Table Name
    protected $table = 'pallets';
    public $primaryKey = 'Pallet_Id';
    public $timestamps = false;

    
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    // Table Name
    protected $table = 'clients';
    public $primaryKey = 'User_Id';
    public $timestamps = false;
}

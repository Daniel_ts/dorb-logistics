<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    // Table Name
    protected $table = 'drivers';
    public $primaryKey = 'User_Id';
    public $timestamps = false;
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Pallet;
use App\User;
use Auth;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('user_id', Auth::id())->get();
        return view('orders.index')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'city' => 'required',
            'street' => 'required',
            'housenumber' => 'required',
            'ZIPCode' => 'required',
            'date' => ['required', 'date']
        ]);
        //Create order
        $order = new Order;
        $order->city = $request->input('city');
        $order->Street = $request->input('street');
        $order->Housenumber = $request->input('housenumber');
        $order->ZIPCode = $request->input('ZIPCode');
        $order->Date = $request->input('date');
        $order->User_Id = Auth::id();
        $order->save();

        

        $PalletContents = $request->input('PalletContents');
        $PalletWeight = $request->input('PalletWeight');
        $x = 0;
        foreach($PalletContents as $PalletContent){
            $pallet = new Pallet;
            $pallet->Order_Id = $order->Order_Id;
            $pallet->Contents = $PalletContent;
            $pallet->Weight = $PalletWeight[$x];
            $pallet->save();
            $x++;
        };

        return redirect('/orders')->with('success', 'Order Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $pallets = Pallet::where('order_id', $id)->get();
        return view('orders.show')->with('order', $order)->with('pallets', $pallets);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $pallets = Pallet::where('order_id', $id)->get();
        return view('orders.edit')->with('order', $order)->with('pallets', $pallets);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'city' => 'required',
            'street' => 'required',
            'housenumber' => 'required',
            'ZIPCode' => 'required',
            'date' => ['required', 'date']
        ]);

        //Update order
        $order = Order::find($id);
        $order->city = $request->input('city');
        $order->Street = $request->input('street');
        $order->Housenumber = $request->input('housenumber');
        $order->ZIPCode = $request->input('ZIPCode');
        $order->Date = $request->input('date');
        $order->save();

        $PalletContents = $request->input('PalletContents');
        $PalletWeight = $request->input('PalletWeight');
        $x = 0;
        
        foreach($PalletContents as $PalletContent){
            if($request->input('new_old.'.$x) == 'old'){
                Pallet::where('Pallet_Id', $request->input('Id.'.$x))->update([
                    'Weight' => $PalletWeight[$x],
                    'Contents' => $PalletContent,
                ]);
            } else {
                $pallet = new Pallet;
                $pallet->Order_Id = $order->Order_Id;
                $pallet->Contents = $PalletContent;
                $pallet->Weight = $PalletWeight[$x];
                $pallet->save();
            }
            $x++;
        };
        return redirect('/orders')->with('success', 'Order Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        return redirect('/orders')->with('success', 'Order Removed');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::User()->Position == 'Client') {
            $orders = Order::where('user_id', Auth::id())->get();
            return view('orders.index')->with('orders', $orders);
        }
        else if(Auth::User()->Position == 'Planner') {
            return view('planner.index');
        } else if(Auth::User()->Position == 'driver'){
            return view('driver.index');
        }
    }
}

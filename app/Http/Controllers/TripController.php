<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Pallet;
use DB;
use Hash;
use App\User;
use App\Trips;
use App\Trucks;
use App\Drivers;
use Auth;
use Carbon\Carbon;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $orders = Order::where('Date', $request->input('date'))->get();
        $drivers = User::where('Position', 'Driver')->join('drivers', 'Users.User_Id', '=', 'Drivers.User_Id')->get();
        $trucks = Trucks::all();
        $pallets = Pallet::all();
        return view('planner.create')->with('date', $request->input('date'))->with('orders', $orders)->with('pallets', $pallets)->with('drivers', $drivers)->with('trucks', $trucks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errorstate = false;
        $trucks = explode(",",$request->input('truck'),2);
        $drivers = explode(",",$request->input('driver'),3);
        if(empty($request->input('orders')) || empty($request->input('truck')) || empty($request->input('driver'))){
            $errorstate = true;
            $errormessage = "Vul alle velden in, tenminste 1 order.";
        } elseif(($drivers[1] == "C" || $drivers[1] == "D") && ($trucks[1] == "CF 300 FA" || $trucks[1] == "CF 410 FTN") || ($drivers[1] == "D" && ($trucks[1] == "CF 300 FA" || $trucks[1] == "CF 410 FTN" || $trucks[1] == "XF 430 FTM"))){
            $x=0;
            foreach($request->input('orders') as $id){
                $pallets = Pallet::where('Order_Id', $id)->get();
                foreach($pallets as $pallet){
                        $x++;
                }
            }
            if(($trucks[1] == "CF 300 FA" && $x <= 10) || ($trucks[1] == "CF 410 FTN" && $x <= 20) || ($trucks[1] == "XF 430 FTM" && $x <= 30)){
                $trips = Trips::where('Date', $request->input('date'))->get();
                foreach($trips as $trip){
                    if($trip->Driver_Id == $drivers[0]){
                        $errorstate = true;
                        $errormessage = "Chauffer al ingepland op datum.";
                    }
                    if($trip->Truck_Id == $trucks[0]){
                        $errorstate = true;
                        $errormessage = "Vrachtwagen al ingepland op datum.";
                    }
                }
                $date = Carbon::createFromFormat('Y-m-d', $request->input('date'));
                Carbon::setTestNow($date);
                $worked = Trips::select(DB::raw('count(Driver_Id) as worked'))->where('Driver_Id', $drivers[0])->whereBetween('date',[Carbon::parse('last monday')->startOfDay(),Carbon::parse('next saturday')->endOfDay()])->groupBy('Driver_Id')->first();
                Carbon::setTestNow();
                if($drivers[2] > $worked['worked'] || empty($worked)){
                    //run code
                } else{
                    $errorstate = true;
                    $errormessage = "Chauffeur werkuren vol.";
                }
                
            } else {
                $errorstate = true;
                $errormessage = "Teveel pallets voor vrachtwagen.";
            }
        } else{
            $errorstate = true;
            $errormessage = "Verkeerde chauffeur voor vrachtwagen.";
        }


        if($errorstate == true){
            $orders = Order::where('Date', $request->input('date'))->get();
            $pallets = Pallet::all();
            $trips = Trips::where('Date', $request->input('date'))->get();
            return redirect('/trips/create?date='.$request->input('date'))->with('orders', $orders)->with('pallets', $pallets)->with('trips', $trips)->with('error', $errormessage);
        } else {
                $trip = new Trips;
                $trip->Date = $request->input('date');
                $trip->Truck_Id = $trucks[0];
                $trip->Driver_Id = $drivers[0];
                $trip->Planner_Id = Auth::id();
                $trip->save();

                foreach ($request->input('orders') as $id){
                    $order = Order::find($id);
                    $order->Trip_Id = $trip->Trip_Id;
                    $order->save();
                }
                return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $orders = Order::where('Date', $request->input('date'))->get();
        $drivers = User::where('Position', 'Driver')->join('drivers', 'Users.User_Id', '=', 'Drivers.User_Id')->get();
        $trucks = Trucks::all();
        $pallets = Pallet::all();
        $trip = Trips::where('Trip_Id', $id)->first();
        return view('planner.edit')->with('date', $request->input('date'))->with('orders', $orders)->with('pallets', $pallets)->with('drivers', $drivers)->with('trucks', $trucks)->with('trip', $trip);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $trucks = explode(",",$request->input('truck'),2);
        $drivers = explode(",",$request->input('driver'),3);
        $trip = Trips::where('Trip_Id', $id)->first();
        $errorstate = false;

        if(empty($request->input('orders')) || empty($request->input('truck')) || empty($request->input('driver'))){
            $errorstate = true;
            $errormessage = "Vul alle velden in, tenminste 1 order.";
        } elseif(($drivers[1] == "C" || $drivers[1] == "D") && ($trucks[1] == "CF 300 FA" || $trucks[1] == "CF 410 FTN") || ($drivers[1] == "D" && ($trucks[1] == "CF 300 FA" || $trucks[1] == "CF 410 FTN" || $trucks[1] == "XF 430 FTM"))){
            $x=0;
            foreach($request->input('orders') as $ip){
                $pallets = Pallet::where('Order_Id', $ip)->get();
                foreach($pallets as $pallet){
                        $x++;
                }
            }
            if(($trucks[1] == "CF 300 FA" && $x <= 10) || ($trucks[1] == "CF 410 FTN" && $x <= 20) || ($trucks[1] == "XF 430 FTM" && $x <= 30)){
                $trips = Trips::where('Date', $request->input('date'))->get();
                foreach($trips as $trip){
                    if($trip->Driver_Id == $drivers[0] && $request->input('predriver') != $drivers[0]){
                        $errorstate = true;
                        $errormessage = "Chauffer al ingepland op datum.";
                    }
                    if($trip->Truck_Id == $trucks[0] && $request->input('pretruck') != $trucks[0]){
                        $errorstate = true;
                        $errormessage = "Vrachtwagen al ingepland op datum.";
                    }
                }
                $date = Carbon::createFromFormat('Y-m-d', $request->input('date'));
                Carbon::setTestNow($date);
                $worked = Trips::select(DB::raw('count(Driver_Id) as worked'))->where('Driver_Id', $drivers[0])->whereBetween('date',[Carbon::parse('last monday')->startOfDay(),Carbon::parse('next saturday')->endOfDay()])->groupBy('Driver_Id')->first();
                Carbon::setTestNow();
                if($drivers[2] > $worked['worked'] || empty($worked) || $trip->Driver_Id == $drivers[0]){
                    //run code
                } else{
                    $errorstate = true;
                    $errormessage = "Chauffeur werkuren vol.";
                }
                
            } else {
                $errorstate = true;
                $errormessage = "Teveel pallets voor vrachtwagen.";
            }
        } else{
            $errorstate = true;
            $errormessage = "Verkeerde chauffeur voor vrachtwagen.";
        }


        if($errorstate == true){
            $orders = Order::where('Date', $request->input('date'))->get();
            $drivers = User::where('Position', 'Driver')->join('drivers', 'Users.User_Id', '=', 'Drivers.User_Id')->get();
            $trucks = Trucks::all();
            $pallets = Pallet::all();

            return redirect('/trips/'.$id.'/edit?date='.$request->input('date'))->with('orders', $orders)->with('pallets', $pallets)->with('drivers', $drivers)->with('trucks', $trucks)->with('trip', $trip)->with('error', $errormessage);
            return view('planner.edit')->with('date', $request->input('date'))->with('orders', $orders)->with('pallets', $pallets)->with('drivers', $drivers)->with('trucks', $trucks)->with('trip', $trip)->with('error', $errormessage);
        } else {
            $trip = Trips::find($request->input('id'));
            $trip->Date = $request->input('date');
            $trip->Truck_Id = $trucks[0];
            $trip->Driver_Id = $drivers[0];;
            $trip->Planner_Id = Auth::id();
            $trip->save();
    
            foreach(Order::where("Trip_Id", $id)->get() as $order){
                $order->Trip_Id = null;
                $order->save();
            }
    
            foreach ($request->input('orders') as $id){
                $order = Order::find($id);
                $order->Trip_Id = $trip->Trip_Id;
                $order->save();
            }
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

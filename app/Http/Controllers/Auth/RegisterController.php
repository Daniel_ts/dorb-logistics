<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:2', 'confirmed'],
            'Tell' => ['required'],
            'Credit_Number' => ['required', 'string', 'max:255'],
            'Street' => ['required', 'string', 'max:255'],
            'City' => ['required', 'string', 'max:255'],
            'ZIPCode' => ['required', 'string', 'max:255'],
            'HouseNumber' => ['required', 'string', 'max:255'],
            'First-Name' => ['required', 'string', 'max:255'],
            'Surname' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'Username' => $data['Username'],
            'password' => Hash::make($data['password']),
            'Position' => 'Client',
            'First_Name' => $data['First-Name'],
            'Surname_Prefix' => $data['Surname-Prefix'],
            'Surname' => $data['Surname'],
        ]);
        $result = DB::table('users')->select('user_id')->where('Username', $data['name'])->first();
        $client = new Client;
        $client->user_id = $result->user_id;
        $client->Email = $data['email'];
        $client->Tell = $data['Tell'];
        $client->Credit_Number = $data['Credit_Number'];
        $client->Street = $data['Street'];
        $client->City = $data['City'];
        $client->ZIPCode = $data['ZIPCode'];
        $client->HouseNumber = $data['HouseNumber'];
        $client->save();

        return $user;
    }
}

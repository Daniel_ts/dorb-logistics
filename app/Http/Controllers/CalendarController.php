<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Pallet;
use DB;
use Hash;
use App\User;
use App\Trips;
use App\Trucks;
use App\Drivers;
use Auth;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::User()->Position == 'Planner'){
            $orders = Order::all();
            foreach($orders as $order){
                if(isset($order->Trip_Id)){
                    $data[] = array(
                        'start' => $order->Date,
                        'title' => 'Order',
                        'color' => 'Green',
                    );
                } else {
                    $data[] = array(
                        'start' => $order->Date,
                        'title' => 'Order',
                        'color' => 'Red',
                    );
                }
                
            }
            return json_encode($data);
        } else if(Auth::User()->Position == 'driver'){
            $trips = Trips::where('Driver_Id', Auth::id())->get();
            foreach($trips as $trip){
                $data[] = array(
                    'start' => $trip->Date,
                    'title' => 'Trip',
                );
            }
            return json_encode($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*$array = array("Q81m6565zb","0V69B7YF8i","rLzng9S89g","R4D4z21pxn","8db75hZh1i","CFGc69o7Kq","5O9rOt5n7P","VN84I3nu2m","xU3YGYwe8D","hpDpq85NuZ","7G6p3sHE7t","PM5hgO3i5s","RQAdU5Rr34","o1U62Eiv84","6SBS3IY8nP","jAWc270F16","GB0rqQG4x2","5pF5AC0IhZ","kuRH1321me","c91u7K28ZA","JF6WO36kEa","kuJr8n5r6d","C038472XFi","bm699iJScZ","i7Pz5NjOe9");
        $vooray = array("Ricky","Kasper","Sjoerd","Luc","Youri","Jan","Joop","Bart","Susan","Laura","Piet","Sjef","Rosa","Henry","Cor","Tom","Bas","Klaas","Sofie","Max","Piet","Bart","Jan","Henk","Rik", "Trudy","Corine","Jaap","Susan","Jop", "Jacqueline","Gerbert");
        $tussray = array("de","","","van","","van de","van","de","","de","de","","","","","","van de","de","de","de","de","","","de","","ter","","","","op den","op den");
        $achtray = array("Groene","Opdam","Hendrix","Engelen","Spa","Berg","Ede","Jong","Havinga","Groot","Klerck","Smits","Veenstra","Burgers","Vink","Schotten","Brink","Boer","Vos","Koning","Leeuw","Vaessen","Jansen","Rechter","Lamers","Linssen","Dal","Gerritsen","Vlierma","Spanjers","Dorb","Dorb");
        $useray = array("r.groene","k.opdam","S.hendrix","l.engelen","y.spa","j.berg","j.ede","o.jong","s.havinga","l.groot","p.klerck","s.smits","r.veenstra","h.burgers","c.vink","t.schotten","b.brink","k.boer","s.vos","m.koning","p.leeuw","b.vaessen","j.jansen","h.rechter","r.lamers","t.linssen","c.dal","j.gerritsen","s.vlierma","j.spanjers","j.dorb","g.dorb");
        $posray = array("driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","driver","planner","planner","planner","planner","planner","manager","manager");
        $x=0;
        foreach ($array as $pw) {
            $hashed = Hash::make($pw);
    
            $user = new User;
                    $user->password = $hashed;
                    $user->First_Name = $vooray[$x];
                    $user->Surname_Prefix = $tussray[$x];
                    $user->Surname = $achtray[$x];
                    $user->Username = $useray[$x];
                    $user->Position = $posray[$x];
                    $user->save();
                    $x++;
        }
        $array = array("BV-45-KS","BX-85-JL","BW-84-KL","BA-14-GF","BP-77-TX","BZ-84-DR","BHT-87-L","BVT-96-R","BPH-44-X","BY-749-B","BF-659-V","BT-83-BP","BT-86-GH","BTT-58-J","BYK-71-S","BGP-56-Z","BBD-10-D","BZ-89-LW","B-845-KH","BWP-32-S");
        $typeray = array("CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 300 FA","CF 410 FTN","CF 410 FTN","CF 410 FTN","CF 410 FTN","CF 410 FTN","CF 410 FTN","XF 430 FTM","XF 430 FTM","XF 430 FTM");
        $x = 0;
        foreach($array as $plate){
            $truck = new Trucks;
            $truck->License_Plate = $plate;
            $truck->Type = $typeray[$x];
            $truck->save();
            $x++;
        }

        $array = array("C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","D","D","D","D","D");
        $typeray = array("EU","EU","EU","EU","EU","EU","NL","NL","NL","NL","NL","BENELUX","BENELUX","BENELUX","BENELUX","BENELUX","NL + B","NL + D","NL + D","NL + D","EU","EU","NL","NL","NL + D");
        $sykeray = array("3","5","2","5","3","2","1","2","5","1","4","5","4","2","2","2","3","5","2","2","4","3","1","5","4");
        $x = 0;
        foreach($array as $lice){
            $driver = new Drivers;
            $driver->User_Id = 81 + $x;
            $driver->License_Type = $lice;
            $driver->License_Nationality = $typeray[$x];
            $driver->Workdays = $sykeray[$x];
            $driver->save();
            $x++;
        }*/
        
        if(Auth::User()->Position == 'Planner') {
            $orders = Order::where('Date', $id)->get();
            $pallets = Pallet::all();
            $trips = Trips::where('Date', $id)->get();
            return view('planner.show')->with('date', $id)->with('orders', $orders)->with('pallets', $pallets)->with('trips', $trips);
        } else if(Auth::User()->Position == 'driver') {
            $trips = Trips::where('Date', $id)->where('Driver_Id', Auth::id())->first();
            if(isset($trips)) {
                $trucks = Trucks::where('Truck_Id', $trips->Truck_Id)->first();
                $orders = Order::where('Date', $id)->where('Trip_Id', $trips->Trip_Id)->get();
                $pallets = Pallet::all();
                return view('driver.show')->with('trips', $trips)->with('orders', $orders)->with('pallets', $pallets)->with('trucks', $trucks);
            } else {
                return view('driver.show')->with('trips', $trips);
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
